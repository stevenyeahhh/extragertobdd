package com.choucair.zimbra.automatizacion2;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/extragerto/vendedores/")
public class RunnerFeatures {

}
