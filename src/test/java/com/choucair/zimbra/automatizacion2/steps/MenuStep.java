package com.choucair.zimbra.automatizacion2.steps;

import com.choucair.zimbra.automatizacion2.pageobjects.MenuPageObject;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Step;

public class MenuStep {
	private MenuPageObject menuPageObject;
	@Step
	public void ingresarNuevo() {
		
		menuPageObject.presionarVentas();
		menuPageObject.presionarVendedores();
		menuPageObject.presionarNuevo();
		
	} 
	@Step
	public void ingresarVendedores() {
		menuPageObject.presionarVentas();
		menuPageObject.presionarVendedores();	
	}
	@Step
	public void buscarVendedor(String cedula) {
		menuPageObject.filtrarVendedor(cedula);
		menuPageObject.buscarVendedor(cedula);
	}
	@Step
	public void validarVendedorExista(String cedula) {
		menuPageObject.filtrarVendedor(cedula);
		menuPageObject.validarVendedor(cedula);
	}
	@Step
	public void cerrarSesion() {
		menuPageObject.cerrarSesion();
	}
}
