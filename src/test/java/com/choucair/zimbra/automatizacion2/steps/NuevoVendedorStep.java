package com.choucair.zimbra.automatizacion2.steps;

import com.choucair.zimbra.automatizacion2.pageobjects.NuevoVendedorPageObject;
import com.choucair.zimbra.automatizacion2.pageobjects.MenuPageObject;

import net.thucydides.core.annotations.Step;

public class NuevoVendedorStep {
	private NuevoVendedorPageObject nuevoVendedorPageObject;
	
	
	public void diligenciarCampos(String documento, String nombre, String direccion, String telefono, String celular,
			String correoElectronico, String basico, String comision) {
		// TODO Auto-generated method stub
		nuevoVendedorPageObject.setTxtDocumento(documento);
		nuevoVendedorPageObject.setTxtNombre(nombre);
		nuevoVendedorPageObject.setTxtDireccion(direccion);
		nuevoVendedorPageObject.setTxtTelefono(telefono);
		nuevoVendedorPageObject.setTxtCelular(celular);
		nuevoVendedorPageObject.setTxtCorreoElectronico(correoElectronico);
		nuevoVendedorPageObject.setTxtBasico(basico);
		nuevoVendedorPageObject.setTxtComision(comision);
		
	}


	public void guardar() {
		// TODO Auto-generated method stub
		nuevoVendedorPageObject.guardarVendedor();
	}
	public void actualizar() {
		// TODO Auto-generated method stub
		nuevoVendedorPageObject.actualizarVendedor();
	}

}
 