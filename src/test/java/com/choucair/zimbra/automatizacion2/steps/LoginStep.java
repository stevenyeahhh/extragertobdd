package com.choucair.zimbra.automatizacion2.steps;




import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import com.choucair.zimbra.automatizacion2.pageobjects.LoginPageObject;

import net.thucydides.core.annotations.Step;




public class LoginStep {

	private LoginPageObject loginPageObject;
	

	public void ingresarExtragerto() {
		// TODO Auto-generated method stub
		loginPageObject.open();
		
	}
	public void iniciarSesionEmpresa(String usuario, String contrasena) {
		// TODO Auto-generated method stub
		loginPageObject.setTxtUsuarioEmpresa(usuario);
		loginPageObject.setTxtContrasenaEmpresa(contrasena);
		loginPageObject.submitBtnEmpresa();
		loginPageObject.verificarCargaEmpresa();
	}
	public void iniciarSesionUsuario(String usuario, String contrasena) {
		// TODO Auto-generated method stub
		loginPageObject.setTxtUsuarioUsuario(usuario);
		loginPageObject.setTxtContrasenaUsuario(contrasena);
		loginPageObject.submitBtnUsuario();
		loginPageObject.verificarCargaUsuario();
	}

}
