package com.choucair.zimbra.automatizacion2.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class MenuPageObject extends PageObject {

	public MenuPageObject(WebDriver driver){
		//PageObject
		super(driver);
		//this.driver=driver;
		
		
		getDriver().switchTo().defaultContent();
	}
	
	
	@FindBy(id="item_5")
	private WebElementFacade moduloVentas;
	@FindBy(id="item_32")
	private WebElementFacade moduloVendedores;
	
	@FindBy(id="sc_b_new_top")
	private WebElementFacade moduloNuevo;
	@FindBy(css="[name='menu_vendedores_iframe']")
	private WebElementFacade vendedoresFrame;
	
	@FindBy(id="item_380")
	private WebElementFacade btnCerrarSesion;
	
	@FindBy(id="sc_b_ins_t")
	private WebElementFacade btnGuardarVendedor;
	@FindBy(css=".scGridFieldOdd")
	private List<WebElement> tbVendedores;
	
	
	public void presionarVentas() {
		// TODO Auto-generated method stub
		//getDriver().switchTo().defaultContent();
		getDriver().switchTo().defaultContent();
		moduloVentas.click();
	}

	public void presionarNuevo() {
		// TODO Auto-generated method stub
		//getDriver().switchTo().frame(vendedoresFrame);
		getDriver().switchTo().frame(getDriver().findElement(By.cssSelector("[name='menu_item_32_iframe']")));
		getDriver().switchTo().frame(getDriver().findElement(By.cssSelector("[name='menu_vendedores_iframe']")));
		getDriver().findElement(By.id("sc_b_new_top")).click();
		//moduloNuevo.click();
	}

	public void presionarVendedores() {
		// TODO Auto-generated method stub
		getDriver().switchTo().defaultContent();
		moduloVendedores.click();
	}

	public void cerrarSesion() {
		// TODO Auto-generated method stub
		getDriver().switchTo().defaultContent();
		btnCerrarSesion.click();
	}

	public void guardarVendedor() {
		// TODO Auto-generated method stub
		getDriver().switchTo().defaultContent();
		btnGuardarVendedor.click();
	}

	public void filtrarVendedor(String cedula) {
		getDriver().switchTo().defaultContent();
		getDriver().switchTo().frame(getDriver().findElement(By.cssSelector("[name='menu_item_32_iframe']")));
		getDriver().switchTo().frame(getDriver().findElement(By.cssSelector("[name='menu_vendedores_iframe']")));
		getDriver().findElement(By.id("SC_fast_search_top")).clear();
		getDriver().findElement(By.id("SC_fast_search_top")).sendKeys(cedula);
		getDriver().findElement(By.id("SC_fast_search_submit_top")).click();
		
	}
	public void buscarVendedor(String cedula) {
		// TODO Auto-generated method stub
		getDriver().switchTo().defaultContent();
		getDriver().switchTo().frame(getDriver().findElement(By.cssSelector("[name='menu_item_32_iframe']")));
		getDriver().switchTo().frame(getDriver().findElement(By.cssSelector("[name='menu_vendedores_iframe']")));
		tbVendedores=getDriver().findElements(By.cssSelector(".scGridTabela tr:not(:first-child)"));
		assertThat(tbVendedores.size(), not(is(0)));
		for (int i = 0; i < tbVendedores.size(); i++) {
			System.out.println(tbVendedores.get(i).getAttribute("innerHTML"));
			List<WebElement> camposVendedores=tbVendedores.get(i).findElements(By.cssSelector("td"));
			if(camposVendedores.get(3).getText().replace(".", "").equals(cedula)) {
				tbVendedores.get(i).findElement(By.id("id_img_bedit")).click();					
			}	
		}		
	}

	public void validarVendedor(String cedula) {
		// TODO Auto-generated method stub
		getDriver().switchTo().defaultContent();
		getDriver().switchTo().frame(getDriver().findElement(By.cssSelector("[name='menu_item_32_iframe']")));
		getDriver().switchTo().frame(getDriver().findElement(By.cssSelector("[name='menu_vendedores_iframe']")));
		tbVendedores=getDriver().findElements(By.cssSelector(".scGridTabela tr:not(:first-child)"));
		
		//asserThat()
		for (int i = 0; i < tbVendedores.size(); i++) {
			List<WebElement> camposVendedores=tbVendedores.get(i).findElements(By.cssSelector("td"));
			assertThat(camposVendedores.get(3).getText().replace(".", ""), is(cedula));
			
		}
	}

}
