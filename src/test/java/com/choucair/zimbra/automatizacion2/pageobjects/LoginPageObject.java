package com.choucair.zimbra.automatizacion2.pageobjects;



import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.List;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.findby.By;

@DefaultUrl("https://app.extragerto.com")
public class LoginPageObject extends PageObject{
	@FindBy(id="cred_userid_inputtext")
	private WebElementFacade txtUsuarioEmpresa;
	
	@FindBy(id="cred_password_inputtext")
	private WebElementFacade txtContrasenaEmpresa;

	@FindBy(id="submit")
	private WebElementFacade  btnSubmitEmpresa;
	

	@FindBy(id="id_sc_field_login")
	private WebElementFacade txtUsuarioUsuario;
	
	@FindBy(id="id_sc_field_pswd")
	private WebElementFacade txtContrasenaUsuario;

	@FindBy(css="button")
	private WebElementFacade  btnSubmitUsuario;

	@FindBy(css="[role='alert']")
	private WebElementFacade bannerEmpresa;
	

	@FindBy(id="idDivMenu")
	private List<WebElementFacade> bannerUsuario;

	public WebElementFacade getTxtUsuarioEmpresa() {
		return txtUsuarioEmpresa;
	}

	public void setTxtUsuarioEmpresa(String txtUsuarioEmpresa) {
		this.txtUsuarioEmpresa.sendKeys(txtUsuarioEmpresa);
	}

	public WebElementFacade getTxtContrasenaEmpresa() {
		return txtContrasenaEmpresa;
	}

	public void setTxtContrasenaEmpresa(String txtContrasenaEmpresa) {
		this.txtContrasenaEmpresa.sendKeys(txtContrasenaEmpresa);
	}

	public WebElementFacade getBtnSubmitEmpresa() {
		return btnSubmitEmpresa;
	}

	public void setBtnSubmitEmpresa(String btnSubmitEmpresa) {
		this.btnSubmitEmpresa.sendKeys(btnSubmitEmpresa);
	}

	public WebElementFacade getTxtUsuarioUsuario() {
		return txtUsuarioUsuario;
	}

	public void setTxtUsuarioUsuario(String txtUsuarioUsuario) {
		this.txtUsuarioUsuario.sendKeys(txtUsuarioUsuario);
	}

	public WebElementFacade getTxtContrasenaUsuario() {
		return txtContrasenaUsuario;
	}

	public void setTxtContrasenaUsuario(String txtContrasenaUsuario) {
		this.txtContrasenaUsuario.sendKeys(txtContrasenaUsuario);
	}

	public WebElementFacade getBtnSubmitUsuario() {
		return btnSubmitUsuario;
	}

	public void setBtnSubmitUsuario(String btnSubmitUsuario) {
		this.btnSubmitUsuario.sendKeys(btnSubmitUsuario);
	}

	public void submitBtnEmpresa() {
		// TODO Auto-generated method stub
		this.btnSubmitEmpresa.click();
	}
	
	public void submitBtnUsuario() {
		// TODO Auto-generated method stub
		this.btnSubmitUsuario.click();
		
		List<WebElement> frameDos= getDriver().findElements(By.id("TB_iframeContent"));
		System.out.println("--------------------");
		System.out.println(frameDos.size());
		if(frameDos.size()!=0) {
			//WebElement frameTotal=
			getDriver().switchTo().frame(frameDos.get(0));
			getDriver().findElement(By.id("sub_form_b")).click();
			
			getDriver().switchTo().defaultContent();
		}
		
	}

	public void verificarCargaEmpresa() {
		// TODO Auto-generated method stub
		getDriver().switchTo().defaultContent();
		assertThat(bannerEmpresa.getText(),containsString("Steven"));
	}
	public void verificarCargaUsuario() {
		// TODO Auto-generated method stub
		assertThat(bannerUsuario.size(),is(1));
	}
	
	
}
