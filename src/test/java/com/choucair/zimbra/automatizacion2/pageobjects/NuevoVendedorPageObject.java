package com.choucair.zimbra.automatizacion2.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class NuevoVendedorPageObject extends PageObject  {
	//private WebElementFacade txt
	
	@FindBy(id="id_sc_field_nit")
	private WebElementFacade txtDocumento;
	@FindBy(id="id_sc_field_nombre")
	private WebElementFacade txtNombre;
	@FindBy(id="id_sc_field_direccion")
	private WebElementFacade txtDireccion;
	@FindBy(id="id_sc_field_telefono")
	private WebElementFacade txtTelefono;
	@FindBy(id="id_sc_field_celular")
	private WebElementFacade txtCelular;
	@FindBy(id="id_sc_field_email")
	private WebElementFacade txtCorreoElectronico;
	@FindBy(id="id_sc_field_basico")
	private WebElementFacade txtBasico;
	@FindBy(id="id_sc_field_comision")
	private WebElementFacade txtComision;
	
	@FindBy(id="sc_b_ins_t")
	private WebElementFacade btnGuardar;
	@FindBy(id="sc_b_sai_t")
	private WebElementFacade btnCancelar;
	
	@FindBy(id="sc_b_upd_t")
	private WebElementFacade btnActualizar;
	
	
	public WebElementFacade getTxtDocumento() {
		return txtDocumento;
	}
	public void setTxtDocumento(String txtDocumento) {
		this.txtDocumento.clear();
		this.txtDocumento.sendKeys(txtDocumento);
	}
	public WebElementFacade getTxtNombre() {
		return txtNombre;
	}
	public void setTxtNombre(String txtNombre) {
		this.txtNombre.clear();
		this.txtNombre.sendKeys(txtNombre);
	}
	public WebElementFacade getTxtDireccion() {
		return txtDireccion;
	}
	public void setTxtDireccion(String txtDireccion) {
		this.txtDireccion.clear();
		this.txtDireccion.sendKeys(txtDireccion);
	}
	public WebElementFacade getTxtTelefono() {
		return txtTelefono;
	}
	public void setTxtTelefono(String txtTelefono) {
		this.txtTelefono.clear();
		this.txtTelefono.sendKeys(txtTelefono);
	}
	public WebElementFacade getTxtCelular() {
		return txtCelular;
	}
	public void setTxtCelular(String txtCelular) {
		this.txtCelular.clear();
		this.txtCelular.sendKeys(txtCelular);
	}
	public WebElementFacade getTxtCorreoElectronico() {
		return txtCorreoElectronico;
	}
	public void setTxtCorreoElectronico(String txtCorreoElectronico) {
		this.txtCorreoElectronico.clear();
		this.txtCorreoElectronico.sendKeys(txtCorreoElectronico);
	}
	public WebElementFacade getTxtBasico() {
		return txtBasico;
	}
	public void setTxtBasico(String txtBasico) {
		this.txtBasico.clear();
		this.txtBasico.sendKeys(txtBasico);
	}
	public WebElementFacade getTxtComision() {
		return txtComision;
	}
	public void setTxtComision(String txtComision) {
		this.txtComision.clear();
		this.txtComision.sendKeys(txtComision);
	}
	public void guardarVendedor() {
		// TODO Auto-generated method stub
		btnGuardar.click();
	}
	public void actualizarVendedor() {
		// TODO Auto-generated method stub
		btnActualizar.click();
	}

	public void cancelar() {
		btnCancelar.click();
	}	
	
}
