package com.choucair.zimbra.automatizacion2.definitions;

import com.choucair.zimbra.automatizacion2.steps.LoginStep;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginDefinition {
	@Steps
	private LoginStep loginStep;

	@Given("^ingresar a Extragerto$")
	public void ingresarExtragerto() {
		loginStep.ingresarExtragerto();
	}
	
	@Given("^iniciar sesión empresa con \"([^\"]*)\" y contraseña \"([^\"]*)\"$")
	public void iniciarSesionEmpresa(String usuario, String contrasena) {
		
		loginStep.iniciarSesionEmpresa(usuario,contrasena);
		
	}
	
	@Given("^iniciar sesión usuario con \"([^\"]*)\" y contraseña \"([^\"]*)\"$")
	public void iniciarSesionUsuario(String usuario, String contrasena) {
		
		loginStep.iniciarSesionUsuario(usuario,contrasena);
		
	}
}
