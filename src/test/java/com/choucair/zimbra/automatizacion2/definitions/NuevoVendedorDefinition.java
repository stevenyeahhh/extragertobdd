package com.choucair.zimbra.automatizacion2.definitions;

import com.choucair.zimbra.automatizacion2.steps.NuevoVendedorStep;
import com.choucair.zimbra.automatizacion2.steps.MenuStep;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class NuevoVendedorDefinition {

	
	@Steps
	private NuevoVendedorStep nuevoVendedorStep;
	
	@When("^diligenciar los campos \"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\"$")
	public void diligenciarCampos(String documento,String nombre, String direccion,String telefono,String celular,String correoElectronico,String basico,String comision) {
		//nuevoVendedorStep.enviar();
		nuevoVendedorStep.diligenciarCampos(documento,nombre,direccion,telefono,celular,correoElectronico,basico,comision);
		
	}
	
	@When("^guardar los datos$")
	public void guardarDatos() {
		nuevoVendedorStep.guardar();
	}
	
	
	@When("^actualizar los datos$")
	public void actualizarDatos() {
		nuevoVendedorStep.actualizar();
	}
	@When("^validar que el vendedor se muestre en la grilla de vendedores$")
	public void validarVendedorRegistrado() {
		
	}
}




