package com.choucair.zimbra.automatizacion2.definitions;

import com.choucair.zimbra.automatizacion2.steps.NuevoVendedorStep;
import com.choucair.zimbra.automatizacion2.steps.MenuStep;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ActulizarVendedorDefinition {
	@Steps
	private NuevoVendedorStep nuevoVendedorStep;
	
	@When("^cambiar los campos \"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\"$")
	public void diligenciarCampos(String documento,String nombre, String direccion,String telefono,String celular,String correoElectronico,String basico,String comision) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		nuevoVendedorStep.diligenciarCampos(documento,nombre,direccion,telefono,celular,correoElectronico,basico,comision);
	}
}
