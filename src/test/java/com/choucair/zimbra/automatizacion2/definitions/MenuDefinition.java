package com.choucair.zimbra.automatizacion2.definitions;

import com.choucair.zimbra.automatizacion2.steps.MenuStep;

import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import cucumber.api.java.en.Then;

public class MenuDefinition {
	@Steps
	private MenuStep menuStep;
	@When("^ingresar al módulo de Ventas->Vendedores->Nuevo$") 
	public void ingresarNuevo() {
		
		menuStep.ingresarNuevo();
		
		
	}
	
	@When("^ingresar al módulo de Ventas->Vendedores$") 
	public void ingresarVendedores() {
		
		menuStep.ingresarVendedores();

		
		
	}
	
	@When("^buscar el vendedor con cédula \"([^\"]*)\"$")
	public void buscarVendedor(String cedula) {
		menuStep.buscarVendedor(cedula);
	}
	@When("^validar que el vendedor con cédula \"([^\"]*)\" exista$")
	public void validarVendedorExista(String cedula) {
		
		menuStep.validarVendedorExista(cedula);
	}
	

	
	@Then("^cerrar sesión$")
	public void cerrarSesion() {
		menuStep.cerrarSesion();
	}

}
