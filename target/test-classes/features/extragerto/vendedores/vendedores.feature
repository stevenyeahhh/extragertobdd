@redactarMensaje
Feature: Vendedores
  
  @rutaCritica
  Scenario: creación de vendedor 
  	Quiero que el sistema me muestre un formulario web
   Given ingresar a Extragerto
     And iniciar sesión empresa con "steven123" y contraseña "steven123"
     And iniciar sesión usuario con "admin" y contraseña "master"
    When ingresar al módulo de Ventas->Vendedores->Nuevo
     And diligenciar los campos "44444441", "vendedor 4","dirección 4","4444444","44444444","1234@a.com", "44444444","44444444"
     And guardar los datos
    Then validar que el vendedor con cédula "44444441" exista
     And cerrar sesión
  @actualizar
  Scenario: actualizar e vendedor 
  	Quiero que el sistema me muestre un formulario web donde pueda actualizar la información de un vendedor
   Given ingresar a Extragerto
     And iniciar sesión empresa con "steven123" y contraseña "steven123"
     And iniciar sesión usuario con "admin" y contraseña "master"
    When ingresar al módulo de Ventas->Vendedores
     And buscar el vendedor con cédula "44444441"  
     And cambiar los campos "44444441", "vendedor modificado","","","","", "",""
     And actualizar los datos
    Then validar que el vendedor con cédula "44444441" exista
     And cerrar sesión
     